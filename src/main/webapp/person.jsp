<%--
  Created by IntelliJ IDEA.
  User: kingk
  Date: 8/19/2020
  Time: 7:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Person Page</title>
</head>
<body>

<div style="font-size: 30px">
<%--    If this JSP was sent an error variable, show it.--%>
    <h5 style="color: red">${error}</h5>
    <hr>
    <h5>Person:</h5>
<%--    Show the person information if a person was provided--%>
    <h3>${person.firstName}</h3>
    <h3>${person.lastName}</h3>
    <h3>${person.age}</h3>
</div>


</body>
</html>
