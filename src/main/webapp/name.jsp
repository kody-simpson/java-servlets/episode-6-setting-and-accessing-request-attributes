<%--
  Created by IntelliJ IDEA.
  User: kingk
  Date: 8/19/2020
  Time: 6:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Name Page</title>
</head>
<body>

<h1>Name Master 2000</h1>

<hr>

<%--The dollar sign thingies is EL (Expression Language)--%>
<%--We will explore it much more in the future of course--%>
<p>Hey ${firstName} ${lastName}, nice to meet you.</p>

</body>
</html>
