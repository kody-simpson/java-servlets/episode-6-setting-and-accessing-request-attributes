import model.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "PersonServlet", urlPatterns = "/person")
public class PersonServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //get the three params we need for a Person
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String age = request.getParameter("age");

        //If any of the params are missing, we will
        //add it to the error
        StringBuilder error = new StringBuilder();
        if (firstName == null){
            error.append("A first name was not provided.");
        }else if (lastName == null){
            error.append("A last name was not provided.");
        }else if (age == null){
            error.append("An age was not provided.");
        }

        //if errors were found, add the error as attribute to be displayed
        if (error.length() > 0){
            request.setAttribute("error", error.toString());
        }else{ //no errors, everything should be here
            //create a person add at it to the request
            Person person = new Person(firstName, lastName, Integer.parseInt(age));
            request.setAttribute("person", person);
        }

        //forward all of the information
        getServletContext().getRequestDispatcher("/person.jsp").forward(request, response);

    }
}
